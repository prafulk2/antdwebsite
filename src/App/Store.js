import { configureStore } from "@reduxjs/toolkit";
import counterReducer from '../Features/Counter/CounterSlice'
import LoginSlicer  from "../Features/Login/LoginSlicer";
import SignupSlicer from "../Features/SignUpData/SignupSlicer";
import Theamslicer from "../Features/Theam/Theamslicer";

export const Store = configureStore({
    reducer: {
        counter: counterReducer,
        theam:Theamslicer,
        login:LoginSlicer,
        signup:SignupSlicer
      },
  })