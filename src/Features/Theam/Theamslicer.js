import React from 'react'
import { createSlice } from '@reduxjs/toolkit'
const initialState={
    color:""
}
export const TheamSlicer = createSlice({
    name: 'counter1',
    initialState,
    reducers: {
      changeTheamColor: (state, action) => {
        state.color = action.payload
      },
    },
  })

  export const {changeTheamColor } = TheamSlicer.actions
  
  export default TheamSlicer.reducer