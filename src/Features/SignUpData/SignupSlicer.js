import React from 'react'
import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    signupData: [],
  }

export const SignupSlicer = createSlice({
    name: 'counter',
    initialState,
    reducers: {
      handleSignUp: (state, action) => {
          console.log(action.payload);
        state.signupData.push(action.payload)
      },
    },
  })
  
  export const { handleSignUp } = SignupSlicer.actions
  
  export default SignupSlicer.reducer
