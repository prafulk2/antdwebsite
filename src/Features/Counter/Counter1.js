import { Button } from 'antd'
import React from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { increment,decrement ,incrementByAmount} from './CounterSlice'

export default function Counter1() {
    const count = useSelector((state)=>state.counter.count)
    const color = useSelector((state)=>state.theam.color)
    const dispatch=useDispatch()
    return (
        <div>
            <Button onClick={()=>{dispatch(increment())}}>inc</Button>
            <p style={{color:`${color}`}}>Counter 1={count}</p>
            <Button onClick={()=>{dispatch(decrement())}}>dec</Button>
            <Button onClick={()=>{dispatch(incrementByAmount(10))}}>incrementByAmount</Button>
        </div>
    )
}
