import { Button } from 'antd'
import React, { useState } from 'react'
import { useSelector,useDispatch } from 'react-redux'



export default function Counter() {
    const [counter, setCounter] = useState(0)
    const count = useSelector((state)=>state.counter.count)
    return (
        <div>
            <Button onClick={()=>setCounter(counter+1)}>inc</Button>
            counter: {counter}
            <Button onClick={()=>setCounter(counter-1)}>dec</Button>
            counter1 count:{count}
        </div>
    )
}
