import React from 'react'
import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    login: false,
  }

export const LoginSlicer = createSlice({
    name: 'counter',
    initialState,
    reducers: {
      handleLogin: (state, action) => {
        state.login = action.payload
      },
    },
  })
  
  export const { handleLogin } = LoginSlicer.actions
  
  export default LoginSlicer.reducer
