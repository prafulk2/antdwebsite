import { Col, Input, Row, Space, Typography } from 'antd'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import Cards from '../Components/Cards'
import './Gallery.scss'
import InfiniteScroll from 'react-infinite-scroll-component';
import { LoadingOutlined } from '@ant-design/icons';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux'

const { Title } = Typography
export default function Gallery() {
    const [data, setData] = useState([])
    const [searchImg, setsearchImg] = useState('')
    const [currentPage, setCurrentPage] = useState(1)
    const logins = useSelector(state => state.login.login)
    
    useEffect(() => {
        window.scrollTo(0,0)
    }, [])
    
    useEffect(() => {
        axios.get(`https://api.unsplash.com/photos/?client_id=cA0qA8zw1r3pXO1hLTK6v8IZDZlNDjXmij-VesHGy2A&page=${currentPage}&per_page=30`)
            .then((res) => {
                setData(res.data)
            })
           
    },[currentPage])
    const ChangeFun = (e) => {
        setsearchImg(e.target.value)
        if (e.target.value === "") {
            axios.get(`https://api.unsplash.com/photos/?client_id=-vYixWTAsxg_dF2LKjEM9pW9G8yYKN5FyOEgeolDlfw&page=${currentPage}&per_page=20`)
                .then((res) => {
                    setData(res.data)
                })
        }
    }
    const searchResult = () => {
        axios.get(`https://api.unsplash.com/search/photos/?client_id=-vYixWTAsxg_dF2LKjEM9pW9G8yYKN5FyOEgeolDlfw&query=${searchImg}&page=${currentPage}&per_page=20`)
            .then((res) => {
                setData(res.data.results)
            })
    }
    const pageChange=() =>{
        if (searchImg) {
            axios.get(`https://api.unsplash.com/search/photos/?client_id=-vYixWTAsxg_dF2LKjEM9pW9G8yYKN5FyOEgeolDlfw&query=${searchImg}&page=${currentPage}&per_page=20`)
                .then((res) => {
                    setData([...data, ...res.data.results])
                })
        }
      
            axios.get(`https://api.unsplash.com/photos/?client_id=cA0qA8zw1r3pXO1hLTK6v8IZDZlNDjXmij-VesHGy2A&page=${currentPage}&per_page=30`)
                .then((res) => {
                    setData([...data, ...res.data])
                })
      
        setCurrentPage(currentPage + 1)
    }
    console.log(data);
    const mid = data.length
    const count1 = Math.ceil(mid / 3)
    const count2 = count1 * 2
    const count3 = count1 * 3
    return (
        <>
            {logins ? "" : <Redirect to="/SignIn" />}
            <Row justify="center" className="GallaryContainer" >
                <Col
                    span="22"
                >
                    <Row justify="center" className="SearchBar">
                        <Col xs={24} xl={20} >
                            <Typography >
                                <Title>
                                    PhotoGallery
                                </Title>
                            </Typography>
                        </Col>
                        <Col xs={24} xl={4} align="end" style={{ alignSelf: "center" }}>
                            <Space>
                                <Input placeholder=" Search Image" onChange={ChangeFun} onPressEnter={searchResult} />
                            </Space>
                        </Col>
                    </Row>
                    <InfiniteScroll
                        dataLength={data.length}
                        next={pageChange}
                        hasMore={true}
                        loader={<Row>
                            <Col span="24" align="center">
                                <div style={{ width: "100px", height: "100px" }}>
                                    <LoadingOutlined style={{ fontSize: "50px" }} />
                                </div>
                            </Col>
                        </Row>}
                    >
                        <Row justify="center" className="sc">
                            <Col>
                                <Row gutter={18}>
                                    <Col xl={8} sm={12} xs={24}>
                                        {data.slice(0, count1).map((value, index) => <Cards obj={value} key={index} /> )}
                                    </Col>
                                    <Col  xl={8} sm={12} xs={24}>
                                        {data.slice(count1, count2).map((value, index) => <Cards obj={value} key={index} />)}
                                    </Col>
                                    <Col xl={8} sm={12} xs={24}>
                                        {data.slice(count2, count3).map((value, index) => <Cards obj={value} key={index} />)}
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </InfiniteScroll>
                </Col>
            </Row>

        </>
    )
}
