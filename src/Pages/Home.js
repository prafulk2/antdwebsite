import { Layout, Col, Row, Typography, Card, Carousel } from 'antd'
import { Redirect } from 'react-router-dom';
import './Home.scss'
import { useContext, useEffect } from 'react';
import { LoginContext } from '../Helper/Context';
import { useSelector } from 'react-redux';

const { Title, Paragraph } = Typography
const { Content } = Layout;
export default function Home() {
    const logins = useSelector(state => state.login.login)

    const images = [
        { url: "https://images.unsplash.com/photo-1586227740560-8cf2732c1531?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=961&q=80" },
        { url: "https://assets-global.website-files.com/5be21acf3359748deb50b2d9/60119e9431d658e36235021c_Header%20Image.jpg" },
        { url: "https://images.unsplash.com/photo-1632008649281-0846891f76bd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1025&q=80" },
        { url: "https://images.unsplash.com/photo-1631772881057-e30d11e6273d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=870&q=80" },
    ]
    useEffect(() => {
        window.scrollTo(0,0)
    },[])
    return (
        <div>
            {logins ? "" : <Redirect to="/SignIn" />}
            <Layout>
                <Layout className="layout">
                    <Content className="contents">
                        <Row className="Row11">
                            <Col >
                                <Carousel autoplay autoplaySpeed={3000} effect="fade" infinite>
                                    {images.map((obj) =>
                                        <div>
                                            <img style={{ objectFit: "cover" }} width="100%" height="500px" src={obj.url} alt="img"/>
                                        </div>
                                    )}
                                </Carousel>,
                            </Col>
                        </Row>
                        <Row justify="center" className="Row2">
                            <Col xl={18} sm={20} xs={24} align="center">
                                <Typography >
                                    <Title level={2} >“Ready To Ditch The HomeAdvisor Pro Badge, Then Create Your Own?”</Title>
                                    <Paragraph style={{ fontSize: "large" }}>Buying leads can be really costly, companies like HomeAdvisor, Thumbtack, & Angie’s List seem to be the only one’s profiting from your services.</Paragraph>
                                </Typography>
                            </Col>
                        </Row>
                        <Row className="Row3" justify="center">
                            <Col span="22">
                                <Row className="subRow1" justify="center">
                                    <Col span="24" align="center" className="Row3Col1">
                                        <Typography className="Row3Typography">
                                            <Title className="title" level={2}>Internet Marketing for Home Service Professionals</Title>
                                        </Typography>
                                    </Col>
                                </Row>
                                <Row className="subRow2" gutter={12} >
                                    <Col sm={24} xs={24} xl={12} className="subRow2Col">
                                        <Typography className="whiteText">
                                            <Paragraph className="whiteTextPara">
                                                Lead generation has never been more difficult when it comes to internet marketing. Everyday there are more home services lead generation
                                                companies coming online. HomeAdvisor, Angie’s List, Thumbtack, Houzz, and now Takl. For General Contractors, Plumbers, HVAC Contractors,
                                                and Remodeling Companies, these leads can cost anywhere from $50 to $350, per lead! In most cases, these leads are sold to several other
                                                competing companies in your area.  Plus there is no guarantee that you will ever even speak with the prospective client!
                                            </Paragraph>
                                        </Typography>
                                    </Col>
                                    <Col sm={24} xs={24} xl={12} className="subRow2Col">
                                        <Typography className="whiteText">
                                            <Paragraph className="whiteTextPara">
                                                For anyone reading this that isn’t already a HomeAdvisor Pro or Thumbtack Pro, this might sound insane. Why on earth would any small
                                                business pay so much in hopes of just talking with a prospective client? The easy answer, well in most cases the return on your
                                                investment, or (ROI),  is still worth it and makes sense for your business. For most businesses, where else are you going to get the
                                                leads, you don’t have any internet presence. Door to door hanging flyers and word of mouth advertising will only go so far.
                                                Don’t worry, when your company is marketed right, you won’t have to be married to these lead generation companies for life!
                                            </Paragraph>
                                        </Typography>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row className="Row4" justify="center">
                            <Col span="22">
                                <Row>
                                    <Col>
                                        <Typography align="center">
                                            <Title level={2}>Not Just an SEO Company, We Offer Full-Service Internet Marketing Services</Title>
                                            <Paragraph style={{ lineHeight: "25px", letterSpacing: "2px" }}>
                                                There is a lot more to an internet marketing service than just putting up a Google My Business page and a website. Lead generation has never been harder for a small business, our company specializes in search engine optimization (SEO), content marketing, Google My Business optimization, and Pay-per-click advertising (PPC), for small mom and pop businesses.
                                            </Paragraph>
                                        </Typography>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row className="Row5" justify="center">
                            <Col span="22">
                                <Row>
                                    <Col align="center">
                                        <Title style={{ lineHeight: "50px", letterSpacing: "2px" }}>“Our Company’s Success Is Only Measured By Your Company’s Success.”</Title>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row className="Row6" justify="center">
                            <Col span="22">
                                <Row justify="center">
                                    <Col align="center">
                                        <Typography>
                                            <Title level="2">
                                                Small Business Marketing
                                            </Title>
                                        </Typography>
                                    </Col>
                                </Row>
                                <Row gutter={[24, 24]}>
                                    <Col xl={6} sm={12} xs={24}>
                                        <Card className="Cards"
                                            style={{ height: "430px" }}
                                            cover={<img height="200" style={{ objectFit: "cover" }} alt="example" src="https://img.freepik.com/free-photo/business-brainstorming-graph-chart-report-data-concept_53876-31213.jpg?size=626&ext=jpg" />}
                                        >

                                            <Typography>
                                                <Title level={3}
                                                    ellipsis={
                                                        {
                                                            rows: 2,
                                                            expandable: false,
                                                            symbol: " "
                                                        }
                                                    }
                                                >
                                                    What Is HomeAdvisor Lead Fee Schedule, How Does it Work?
                                                </Title>
                                                <Paragraph
                                                    ellipsis={
                                                        {
                                                            rows: 4,
                                                            expandable: true,
                                                            symbol: " "
                                                        }
                                                    }
                                                >
                                                    HomeAdvisor is one of the most popular lead generation companies for contractors and other service providers. The company helps dramatically improve the online marketin,HomeAdvisor is one of the most popular lead generation companies for contractors and other service providers.The company helps dramatically improve the online marketin
                                                </Paragraph>
                                            </Typography>

                                        </Card>
                                    </Col>
                                    <Col xl={6} sm={12} xs={24}>
                                        <Card className="Cards"
                                            style={{ height: "430px" }}
                                            cover={<img height="200" style={{ objectFit: "cover" }} alt="example" src="https://www.investopedia.com/thmb/5_l_gNEBaEfz10PhdqexDMklsAo=/1333x1000/smart/filters:no_upscale()/GettyImages-658647254-5468427ab5c94e9b82f9132e60aad7a0.jpg" />}
                                        >
                                            <Typography>
                                                <Title level={3}
                                                    ellipsis={
                                                        {
                                                            rows: 2,
                                                            expandable: false,
                                                            symbol: " "
                                                        }
                                                    }
                                                >
                                                    What Does a Google Business Manager Do?
                                                </Title>
                                                <Paragraph
                                                    ellipsis={
                                                        {
                                                            rows: 4,
                                                            expandable: true,
                                                            symbol: " "
                                                        }
                                                    }
                                                >
                                                    A Google Business Manager is a profile for your business or service that your company provides. In many cases, this business is the one-stop-shop in Manager is a profile for your business or service that your company provides. In many cases, this business is the one-stop-shop .
                                                </Paragraph>
                                            </Typography>
                                        </Card>
                                    </Col>
                                    <Col xl={6} sm={12} xs={24}>
                                        <Card className="Cards"
                                            style={{ height: "430px" }}
                                            cover={<img alt="example" height="200" style={{ objectFit: "cover" }} src="https://images.pexels.com/photos/3184418/pexels-photo-3184418.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" />}
                                        >
                                            <Typography>
                                                <Title level={3}
                                                    ellipsis={
                                                        {
                                                            rows: 2,
                                                            expandable: false,
                                                            symbol: " "
                                                        }
                                                    }
                                                >
                                                    Internet Marketing Company vs. Digital Marketing Agency
                                                </Title>
                                                <Paragraph
                                                    ellipsis={
                                                        {
                                                            rows: 4,
                                                            expandable: true,
                                                            symbol: " "
                                                        }
                                                    }
                                                >
                                                    Digital marketing promotes, improves sales, and uses online marketing tactics such as social media marketing, search marketing, and email marketing to promote products and services,
                                                    Digital marketing promotes, improves sales, and uses online marketing tactics such as social media marketing, search marketing, and email marketing to promote products and services
                                                </Paragraph>
                                            </Typography>
                                        </Card>
                                    </Col>
                                    <Col xl={6} sm={12} xs={24}>
                                        <Card className="Cards"
                                            style={{ height: "430px" }}
                                            cover={<img alt="example" height="200" style={{ objectFit: "cover" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQI0ZEZCQNH2-k7XR95vcg_EJKiFb2DXzn5Fw&usqp=CAU" />}
                                        >
                                            <Typography>
                                                <Title level={3}
                                                    ellipsis={
                                                        {
                                                            rows: 2,
                                                            expandable: false,
                                                            symbol: " "
                                                        }
                                                    }
                                                >
                                                    How to Find an Agency for Digital Marketing
                                                </Title>
                                                <Paragraph
                                                    ellipsis={
                                                        {
                                                            rows: 4,
                                                            expandable: true,
                                                            symbol: " "
                                                        }
                                                    }
                                                >
                                                    Companies spend more than ever on digital ads for their media budgets, and it would be wise for everybody to consider investing in it.
                                                    Companies spend more than ever on digital ads for their media budgets, and it would be wise for everybody to consider investing in it.
                                                </Paragraph>
                                            </Typography>
                                        </Card>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Content>
                </Layout>
            </Layout>
        </div>
    )
}
