import React, { useContext, useEffect } from 'react'
import { Button, Typography, Col, Form, Input, Row } from 'antd'
import './Contact.scss'
import { LoginContext } from '../Helper/Context';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const { TextArea } = Input
const { Title } = Typography;
export default function ContactUs() {
    const logins = useSelector(state => state.login.login)
    useEffect(() => {
        window.scrollTo(0,0)
    },[])

    return (
        <>
            {logins ? "" : <Redirect to="/SignIn" />}
            <Row className="contactCon1" >
                <Col xl={10} sm={24} xs={24} >
                    <Row justify="center">
                        <Col xl={20} sm={14} xs={22}>
                            <Row className="Row1">
                                <Col span="24">
                                    <Row justify="center">
                                        <Col>
                                            <Title style={{ paddingTop: "20px", fontSize: "40px", fontWeight: "bold", color: "rgb(80, 84, 226)" }}>
                                                Contact Us
                                            </Title>
                                        </Col>
                                    </Row>
                                    <Row justify="center">
                                        <Col span="24">
                                            <Form className="form"
                                                layout="vertical"
                                            >
                                                <Row justify="center" className="FormRow">
                                                    <Col
                                                        span="22"
                                                    >
                                                        <Form.Item
                                                            hasFeedback
                                                            name="Name"
                                                            rules={[
                                                                {
                                                                    required: true,
                                                                    message: "Please enter a Name"
                                                                },
                                                                {
                                                                    max: 20,
                                                                    message: "Name should be maximum 20 charectr long"
                                                                },
                                                                {
                                                                    min: 6,
                                                                    message: "Minimum 5 charcter required"
                                                                }
                                                            ]}
                                                        >
                                                            <Input className="input" placeholder="Name" />
                                                        </Form.Item>
                                                        <Form.Item
                                                            name="Email"
                                                            rules={[
                                                                {
                                                                    required: "true",
                                                                    message: "Please enter a Email"
                                                                },
                                                                {
                                                                    type: 'email',
                                                                    message: "Enter valid Email"
                                                                },
                                                            ]}
                                                            hasFeedback
                                                        >
                                                            <Input className="input" placeholder="Email" />
                                                        </Form.Item>
                                                        <Form.Item

                                                            name="Message"
                                                            hasFeedback
                                                            rules={[
                                                                {
                                                                    required: true,
                                                                    message: "Please enter a Message"
                                                                },
                                                                {
                                                                    max: 100,
                                                                    message: "Message should be maximum 20 character long"
                                                                },
                                                                {
                                                                    min: 20,
                                                                    message: "Message at least 20 character"
                                                                },

                                                            ]}
                                                        >
                                                            <TextArea
                                                                className="input"
                                                                placeholder="Message"
                                                                autoSize={{ minRows: 3, maxRows: 5 }}
                                                                maxLength={100} />
                                                        </Form.Item>
                                                        <Row justify="center">
                                                            <Col span="24" align="center">
                                                                <Button type="primary" htmlType="submit" className="button">
                                                                    Submit
                                                                </Button>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </Form>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}
