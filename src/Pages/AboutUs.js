import React, { useContext, useEffect } from 'react'
import { Button, Typography, Col, Row,Card } from 'antd'
import './AboutUs.scss'
import { Redirect } from 'react-router-dom';
import { LoginContext } from '../Helper/Context';
import { useSelector } from 'react-redux';

const { Title, Paragraph } = Typography;
export default function AboutUs() {
    const logins = useSelector(state => state.login.login)
    useEffect(() => {
        window.scrollTo(0,0)
    },[])
    return (
        <>
            {logins ? "" : <Redirect to="/SignIn" />}
            <Row className="AboutUsContainer" justify="center" >
                <Col span="22" >
                    <Row className="Rows1">
                        <Col xl={10} >
                            <Row justify="center">
                                <Col sm={24}>
                                    <Typography className="TypographyRows1">
                                        <Title className="ty1rows1Title">
                                            ABOUT US
                                        </Title>
                                        <Paragraph className="ty1rows1Para">
                                            A Web guy saw the future in Mobile to begin with. On a cozy couch an idea was born of Mobile Development knowing that it is going to become a niche in the Software development industry in near future. Single handedly he started working on that one project balancing two different roles. Project Management and delivery by day and insomniac coding at night. Who knew that someday it will be written down in the eureka moment for the existence of WebMob Technologies
                                        </Paragraph>
                                    </Typography>
                                    <Button className="btn">
                                        Learn More
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="Rows2" justify="center">
                <Col span="22">
                    <Row justify="center">
                        <Col xl={16} xs={24} align="center">
                            <Typography className="TypographyRows1">
                                <Paragraph className="ty1rows1Para">
                                    Odhav Web Technologies is an expeditiously growing Indian company based in Ahmedabad, India, believed to be one of the popular providers of IT services. The company is determined to offer more achievable results and solutions to various businesses confronting challenges in the Information Technology Sector.
                                </Paragraph>
                            </Typography>
                        </Col>
                    </Row>
                    <Row justify="center">
                        <Col xl={20} xs={24} align="center">
                            <Typography className="TypographyRows1">
                                <Paragraph className="ty1rows1Para">
                                    We are a team of young professionals, led by experts, who have come together and are willing to go beyond their reach to work on an audacious mission- <b>to equip the world with digital services, right at their door step</b>. The company focuses on skill mastery to help every individual or business establish strong foundations by providing them with high-quality services that are relevant as per the market needs at their door step.
                                </Paragraph>
                            </Typography>
                        </Col>
                    </Row>
                    <Row justify="center">
                        <Col xl={16} xs={24} align="center">
                            <Typography className="TypographyRows1">
                                <Paragraph className="ty1rows1Para">
                                    We at Ohho Web Technologies passionately believe in spreading awareness regarding the importance of digitalization where everyone is connected with each other globally.
                                </Paragraph>
                            </Typography>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="Rows3" justify="center">
                <Col span="22">
                    <Row justify="center">
                        <Col align="center">
                            <Typography className="TypographyRows1">
                                <Paragraph className="ty1rows1Para">
                                    OUR SERVICE
                                </Paragraph>
                                <Title className="ty1rows1Title">
                                    How We Can Help?
                                </Title>
                            </Typography>
                        </Col>
                    </Row>
                    <Row >
                        <Col span="24">
                            <Row gutter={[24, 24]}>
                                <Col
                                    xl={8}
                                    sm={12}
                                    xs={24}
                                >
                                    <Card
                                        className="cards"
                                    >
                                        <Row gutter={10}>
                                            <Col span="4" >
                                                <img alt="seo" style={{ width: "100%" }} src="	https://web999.in/wp-content/uploads/2021/01/home10_service1-active.png" />
                                            </Col>
                                            <Col span="16">
                                                <Title level={3}
                                                    ellipsis={{
                                                        rows: 1
                                                    }}
                                                >SEO Optimization</Title>
                                            </Col>
                                        </Row>
                                        <Row style={{ padding: "1rem 0" }}>
                                            <Col span="18" >
                                                <Paragraph
                                                    ellipsis={{
                                                        rows: 3
                                                    }}
                                                    style={{ fontSize: "large" }}>
                                                    Get more website traffic, more customers, and more online visibility.
                                                </Paragraph>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col
                                    xl={8}
                                    sm={12}
                                    xs={24}
                                >
                                    <Card
                                        className="cards"
                                    >
                                        <Row gutter={10}>
                                            <Col span="4" >
                                                <img alt="seo" style={{ width: "100%" }} src="https://web999.in/wp-content/uploads/2021/01/home10_service2-active.png" />
                                            </Col>
                                            <Col span="16">
                                                <Title level={3}
                                                    ellipsis={{
                                                        rows: 1
                                                    }}
                                                >Social Media</Title>
                                            </Col>
                                        </Row>
                                        <Row style={{ padding: "1rem 0" }}>
                                            <Col span="18" >
                                                <Paragraph
                                                    ellipsis={{
                                                        rows: 3
                                                    }}
                                                    style={{ fontSize: "large" }}>
                                                    Create and manage top-performing social campaigns and start.
                                                </Paragraph>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col
                                    xl={8}
                                    sm={12}
                                    xs={24}
                                >
                                    <Card
                                        className="cards"
                                    >
                                        <Row gutter={10}>
                                            <Col span="4" >
                                                <img alt="seo" style={{ width: "100%" }} src="https://web999.in/wp-content/uploads/2021/01/home10_service3-active.png" />
                                            </Col>
                                            <Col span="16">
                                                <Title level={3}
                                                    ellipsis={{
                                                        rows: 1
                                                    }}
                                                >Web Design</Title>
                                            </Col>
                                        </Row>
                                        <Row style={{ padding: "1rem 0" }}>
                                            <Col span="18" >
                                                <Paragraph
                                                    ellipsis={{
                                                        rows: 3
                                                    }}
                                                    style={{ fontSize: "large" }}>
                                                    Create, publish, and promote engaging content to generate more traffic.
                                                </Paragraph>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col
                                    xl={8}
                                    sm={12}
                                    xs={24}
                                >
                                    <Card
                                        className="cards"
                                    >
                                        <Row gutter={10}>
                                            <Col span="4" >
                                                <img alt="seo" style={{ width: "100%" }} src="	https://web999.in/wp-content/uploads/2021/01/home10_service4-active.png" />
                                            </Col>
                                            <Col span="16">
                                                <Title level={3}
                                                    ellipsis={{
                                                        rows: 1
                                                    }}
                                                >PPC Advertisign</Title>
                                            </Col>
                                        </Row>
                                        <Row style={{ padding: "1rem 0" }}>
                                            <Col span="18" >
                                                <Paragraph
                                                    ellipsis={{
                                                        rows: 3
                                                    }}
                                                    style={{ fontSize: "large" }}>
                                                    Target your ideal search phrases and get found at the top of search results. PPC allows you.
                                                </Paragraph>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col
                                    xl={8}
                                    sm={12}
                                    xs={24}
                                >
                                    <Card
                                        className="cards"
                                    >
                                        <Row gutter={10}>
                                            <Col span="4" >
                                                <img alt="seo" style={{ width: "100%" }} src="https://web999.in/wp-content/uploads/2021/01/home10_service5-active.png" />
                                            </Col>
                                            <Col span="16">
                                                <Title level={3}
                                                    ellipsis={{
                                                        rows: 1
                                                    }}
                                                >Keyword Research</Title>
                                            </Col>
                                        </Row>
                                        <Row style={{ padding: "1rem 0" }}>
                                            <Col span="18" >
                                                <Paragraph
                                                    ellipsis={{
                                                        rows: 3
                                                    }}
                                                    style={{ fontSize: "large" }}>
                                                    We select themed keywords based on user-intent to solidify rankings based on what users searches
                                                </Paragraph>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col
                                    xl={8}
                                    sm={12}
                                    xs={24}
                                >
                                    <Card
                                        className="cards"
                                    >
                                        <Row gutter={10}>
                                            <Col span="4" >
                                                <img alt="seo" style={{ width: "100%" }} src="	https://web999.in/wp-content/uploads/2021/01/home10_service6-active.png" />
                                            </Col>
                                            <Col span="16">
                                                <Title level={3}
                                                    ellipsis={{
                                                        rows: 1
                                                    }}
                                                >Lightning Hosting</Title>
                                            </Col>
                                        </Row>
                                        <Row style={{ padding: "1rem 0" }}>
                                            <Col span="18" >
                                                <Paragraph
                                                    ellipsis={{
                                                        rows: 3
                                                    }}
                                                    style={{ fontSize: "large" }}>
                                                    Our web application accelerator, powered by Varnish Cache, ensures the maximum performance of your website at all times!
                                                </Paragraph>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>

                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}
