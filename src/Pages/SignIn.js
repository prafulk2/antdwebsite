import React, { useState, useContext, useEffect } from 'react'
import { Button, Typography, Col, Form, Input, Row, message, Checkbox } from 'antd'
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { handleLogin } from '../Features/Login/LoginSlicer';

const { Title } = Typography;
export default function SignIn() {
    const [form] = Form.useForm()
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')
    const logins = useSelector(state => state.login.login)
    const signup = useSelector(state => state.signup.signupData)
    const dispatch=useDispatch()

    console.log(logins);
    useEffect(() => {
        window.scrollTo(0,0)
    },[])
    const hadleField = (e) => {
        switch (e.target.name) {
            case "username":
                setName(e.target.value)
                break;
            case "password":
                setPassword(e.target.value)
                break;
            default:
                break;
        }
    }
    const unlabled = {
        wrapperCol: {
            xs: { offset: 0, span: 0 },
            sm: { offset: 4, span: 16 },
            xl: { offset: 3, span: 18 }
        }
    }
    const validateField = (_, value) => {
        const regrx = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/
        return regrx.test(value) ? Promise.resolve() : Promise.reject(new Error("only alphabet allow"))
    }
    const formFinish = () => {
        // let userData = []    
        // userData = JSON.parse(localStorage.getItem('userData')) ? JSON.parse(localStorage.getItem('userData')) : []
        if (signup.some((v) => v.name === name && v.pass === password)) {
            dispatch(handleLogin(true))
        }
        else {
            message.error("username or password not valid")
        }
    }
    return (
        <>
            {logins ? <Redirect to="/Home" /> : ""}
            <div className="formContiner">
                <div className="FormRow">
                    <Row justify="center" className="" >
                        <Col className="FormCol" xs={22} sm={15} xl={7} >
                            <div style={{ textAlign: "center" }}>
                                <Title className="title" level={2}>Login To Your Account</Title>
                            </div>
                            <Form
                                onFinish={formFinish}
                                form={form}
                            >
                                <Form.Item
                                    {...unlabled}
                                    name="usename"
                                    hasFeedback
                                    rules={[
                                        {
                                            required: true,
                                            message: "Please enter a username"
                                        },
                                        {
                                            max: 20,
                                            message: "Username should be maximum 20 charectr long"
                                        },
                                        {
                                            min: 6,
                                            message: "Minimum 5 charcter required"
                                        },
                                        {
                                            validator: validateField
                                        }
                                    ]}
                                >
                                    <Input placeholder="Username" name="username" value={name} onChange={hadleField} />
                                </Form.Item>
                                <Form.Item
                                    hasFeedback
                                    {...unlabled}
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Please enter a password"
                                        },
                                        {
                                            min: 8,
                                            message: "minimum 8 digit required"
                                        }
                                    ]}
                                >
                                    <Input.Password placeholder="Password" name="password" value={password} onChange={hadleField} />
                                </Form.Item>
                                <Form.Item
                                    {...unlabled}>
                                    <Form.Item name="remember" valuePropName="checked" noStyle>
                                        <Checkbox style={{ float: "left" }}>Remember me</Checkbox>
                                    </Form.Item>
                                </Form.Item>
                                <Form.Item
                                    {...unlabled}
                                >
                                    <Button type="primary" htmlType="submit" style={{ width: "100%" }}>Sign in</Button>
                                </Form.Item>
                                <Form.Item
                                    {...unlabled}
                                >
                                    <div style={{ textAlign: "center" }}>
                                        Need an account? <Link to="/SignUp">Sign Up</Link>
                                    </div>
                                </Form.Item>
                            </Form>
                        </Col>
                    </Row>
                </div>
            </div>
        </>
    )
}
