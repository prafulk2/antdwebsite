import React, { useEffect, useState } from 'react'
import { Button, Typography, Col, Form, Input, Row, message, Checkbox } from 'antd'
import "./SignIn.scss"
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { handleSignUp } from '../Features/SignUpData/SignupSlicer';


const { Title } = Typography;
export default function SignUp() {
    const [form] = Form.useForm()
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState()

    const signup = useSelector(state => state.signup.signupData)
    const dispatch = useDispatch()
    console.log(signup);
    useEffect(() => {
        window.scrollTo(0,0)
    }, [])
    const unlabled = {
        wrapperCol: {
            xs: { offset: 0, span: 0 },
            sm: { offset: 4, span: 16 },
            xl: { offset: 2, span: 20 }
        }
    }
    const hadleField = (e) => {
        switch (e.target.name) {
            case "username":
                setName(e.target.value)
                break;
            case "email":
                setEmail(e.target.value)
                break;
            case "password":
                setPassword(e.target.value)
                break;
            default:
                break;
        }
    }
    const formFinish = () => {
        // let userData = []
        // userData = JSON.parse(localStorage.getItem('userData')) ? JSON.parse(localStorage.getItem('userData')) : []
        if (signup.some((v) =>{ return v.name === name || v.email === email })) {
            message.error("username or email alredy exist")
        }
        else {
            message.success("form submited")
            // form.resetFields()
            // userData.push({
            //     "name": name,
            //     "email": email,
            //     "pass":password
            // })
            // localStorage.setItem('userData', JSON.stringify(userData));
            // console.log(userData);
            dispatch(handleSignUp({
                "name": name,
                "email": email,
                "pass":password
            }))
        }
    }




    const validateField = (_,value)=>{

        const regrx=/^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/
        let str = value
        return regrx.test(str)?Promise.resolve():Promise.reject(new Error("only alphabet allow"))
    }
    return (
        <>
            <div className="formContiner">
                <div className="FormRow">
                    <Row justify="center" className="" >
                        <Col className="FormCol" xs={22} sm={15} xl={7} >
                            <div style={{ textAlign: "center" }}>
                                <Title className="title" level={2}>Create Your Account</Title>
                            </div>
                            <Form
                                onFinish={formFinish}
                                form={form}
                            >
                                <Form.Item
                                    {...unlabled}
                                    name="usename"
                                    hasFeedback
                                    rules={[
                                        {
                                            required: true,
                                            message: "Please enter a username"
                                        },
                                        {
                                            max: 20,
                                            message: "Username should be maximum 20 charectr long"
                                        },
                                        {
                                            min: 6,
                                            message: "Minimum 5 charcter required"
                                        },
                                        {
                                            validator:validateField
                                        }
                                    ]}
                                >
                                    <Input placeholder="Username" name="username" value={name} onChange={hadleField} />
                                </Form.Item>
                                <Form.Item
                                    {...unlabled}
                                    name="email"
                                    hasFeedback
                                    rules={[
                                        {
                                            required: true,
                                            message: "Please enter a Email"
                                        },
                                        {
                                            type: 'email',
                                            message: "please enter a valid email"
                                        }
                                    ]}
                                >
                                    <Input placeholder="Email" name="email" value={email} onChange={hadleField} />
                                </Form.Item>
                                <Form.Item
                                    hasFeedback
                                    {...unlabled}
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Please enter a password"
                                        },
                                        {
                                            min:8,
                                            message:"minimum 8 digit required"
                                        }

                                    ]}
                                >
                                    <Input.Password placeholder="Password" name="password"   value={password} onChange={hadleField}/>

                                </Form.Item>
                                <Form.Item
                                    hasFeedback
                                    {...unlabled}
                                    name="confirm"
                                    dependencies={['password']}
                                    rules={[
                                        {
                                            required: true,
                                            message: "Please enter a confirm password"
                                        },
                                        ({ getFieldValue }) => ({
                                            validator(_, value) {
                                                if (!value || getFieldValue('password') === value) {
                                                    return Promise.resolve();
                                                }
                                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                            },
                                        })
                                    ]}
                                >
                                    <Input.Password placeholder="Confirm Password" />

                                </Form.Item>
                                <Form.Item
                                    {...unlabled}
                                >
                                    <Checkbox style={{ float: "left" }}>I accept the <a href="#https//:">Terms of Use </a>&<a href="#https//:"> Privacy Policy</a></Checkbox>
                                </Form.Item>
                                <Form.Item
                                    {...unlabled}
                                >
                                    <Button type="primary" htmlType="submit" style={{ width: "100%" }}>Sign Up</Button>
                                </Form.Item>
                                <Form.Item
                                    {...unlabled}
                                >
                                    <div style={{ textAlign: "center" }}>
                                        Already have an account? <Link to="/SignIn">Sign In</Link>
                                    </div>
                                </Form.Item>
                            </Form>

                        </Col>
                    </Row>
                </div>

            </div>
        </>
    )
}
