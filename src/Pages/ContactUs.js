// import React from 'react'
// import { Button, Typography, Col, Form, Input, Row, InputNumber, Divider, Select } from 'antd'
// import './ContactUs.scss'


// const { Option } = Select;
// const { TextArea } = Input
// const { Title, Paragraph } = Typography;
// export default function ContactUs() {
//     const [form] = Form.useForm()
//     // const validateFun=(_,val)=>{
//     //     let str=String(val)
//     //     console.log(str.split('').length);
//     // }
//     const citys = ['Mumbai', 'New Delhi', 'Bangalore', 'Hyderabad', 'Ahmedabad', 'Chennai', 'Kolkata', 'Surat', 'Pune']
//     const Options = citys.map((val, index) =>
//         <Option key={index}>{val}</Option>
//     )
//     return (
//         <>
//             <Row className="Row1" justify="center">
//                 <Col>
//                          <Typography className="ContactUs">
//                         <Title level={1} style={{ padding: "6rem 0",color:"white" }}>
//                             Contact us
//                         </Title>
//                     </Typography>


//                 </Col>
//             </Row>
//             <Row className="conContainer" justify="center">
//                 <Col span="22">
//                     <Row justify="center" className="Row2">
//                         <Col align="center">
//                             <Title level={2} style={{marginTop:"3rem"}}>“You miss 100% of the shots you don’t take.“</Title>
//                         </Col>
//                     </Row>
//                     <Row className="Row3">
//                         <Col>
//                             <Typography >
//                                 <Title align="center" level={2} style={{color:"rgb(18, 86, 157)"}}>Odhav Gramph</Title>
//                                 <Paragraph style={{ fontSize: "16px", lineHeight: "34px" }}>We get it, nobody wants to contact an SEO Company, you never know what a salesman might answer, right? Creactive is a small marketing company that wants you to feel at home. We are a full-service digital marketing agency that has a masters in search engine optimization. In most cases you will be working directly with one of the owners, we don’t use high-pressure sales tactics, and foremost, we want you to be comfortable with your decision. So reach out to us, give us a call or send us an email, you won’t be disappointed!</Paragraph>
//                             </Typography>
//                         </Col>
//                     </Row>
//                     <Row justify="center" className="Row4">
//                         <Col>
//                             <Title level={3} style={{ marginBottom: "2rem" }} align="center">Work With The #1 Small Business SEO Company in The India</Title>
//                         </Col>
//                     </Row>
//                     <Divider style={{ borderTop: "1px solid black" ,marginTop:"0"}}></Divider>
//                     <Row gutter={16} justify="center">
//                         <Col>
//                             <Title level={4}>
//                                 555-345-1232
//                             </Title>
//                         </Col>
//                         <Col>
//                             <Title level={4}>
//                                 653-233-9804
//                             </Title>
//                         </Col>
//                     </Row>
//                     <Row justify="center">
//                         <Col xl={24} sm={20} xs={24}>
//                             <Form
//                                 layout="vertical"
//                                 form={form}
//                             >
//                                 <Row gutter="16">
//                                     <Col xl={12} sm={12} xs={24}>
//                                         <Form.Item
//                                             label="First Name"
//                                             name="FirstName"
//                                             rules={[
//                                                 {
//                                                     required: true
//                                                 }
//                                             ]}
//                                         >
//                                             <Input className="inputformfield" placeholder="First Name" />
//                                         </Form.Item>
//                                     </Col>
//                                     <Col xl={12} sm={12} xs={24}>
//                                         <Form.Item
//                                             label="Last Name"
//                                             name="LastName"
//                                             rules={[
//                                                 {
//                                                     required: true
//                                                 }
//                                             ]}
//                                         >
//                                             <Input className="inputformfield" placeholder="Last Name" />
//                                         </Form.Item>
//                                     </Col>
//                                 </Row>
//                                 <Row gutter="16">
//                                     <Col xl={12} sm={12} xs={24}>
//                                         <Form.Item
//                                             label="Phone Number"
//                                             name="Phone Number"
//                                             rules={[
//                                                 {
//                                                     required: true
//                                                 },
//                                                 {
//                                                     type: "number",

//                                                 }
//                                             ]}
//                                         >
//                                             <InputNumber className="inputformfield" minLength={10} min={0} maxLength={10} style={{ width: "100%" }} placeholder="Phone Number" />
//                                         </Form.Item>
//                                     </Col>
//                                     <Col xl={12} sm={12} xs={24}>
//                                         <Form.Item
//                                             label="City"
//                                             name="city"
//                                             rules={[
//                                                 {
//                                                     required: true
//                                                 }
//                                             ]}
//                                         >
//                                             <Input className="inputformfield" placeholder="City" />
//                                         </Form.Item>
//                                     </Col>
//                                 </Row>

//                                 <Row gutter="16">
//                                     <Col xl={12} sm={12} xs={24}>
//                                         <Form.Item
//                                             label="State"
//                                             name="state"
//                                             rules={[
//                                                 {
//                                                     required: true
//                                                 },
//                                             ]}
                                        
//                                         >
//                                             <Select
//                                                         style={
//                                                             {
//                                                                 height:"45px",
//                                                                 border:"1px solid black"
//                                                             }
//                                                         }
//                                                 placeholder="State select"
//                                                 defaultValue={['Mumbai']}
//                                             >
//                                                 {Options}
//                                             </Select>
//                                         </Form.Item>
//                                     </Col>
//                                     <Col xl={12} sm={12} xs={24}>
//                                         <Form.Item
//                                             label="Zip Code"
//                                             name="zipcode"
//                                             rules={[
//                                                 {
//                                                     required: true
//                                                 },
//                                                 {
//                                                     type: "number",

//                                                 }
//                                             ]}
//                                         >
//                                             <InputNumber className="inputformfield" minLength={6} min={0} maxLength={6} style={{ width: "100%" }} placeholder="Zip Code" />
//                                         </Form.Item>
//                                     </Col>
//                                 </Row>
//                                 <Row>
//                                     <Col span="24">
//                                         <Form.Item
//                                             label="Business Name"
//                                             name="BusinessName"
//                                         >
//                                             <Input className="inputformfield" placeholder="Business Name"/>
//                                         </Form.Item>
//                                     </Col>
//                                 </Row>
//                                 <Row>
//                                     <Col span="24">
//                                         <Form.Item
//                                             label="Email"
//                                             name="email"
//                                             rules={[
//                                                 {
//                                                     required:true,
//                                                     message:"Email Required"
//                                                 },
//                                                 {
//                                                     type:"email",
//                                                     message:"Enter Valid Email"
//                                                 }
//                                             ]}
//                                         >
//                                             <Input className="inputformfield" placeholder="Email"/>
//                                         </Form.Item>
//                                     </Col>
//                                 </Row>
//                                 <Row>
//                                     <Col span="24">
//                                     <Form.Item
//                                             label="Notes"
//                                             name="notes"
//                                             hasFeedback
//                                             rules={[
//                                                 {
//                                                     required: true,
//                                                     message: "Please enter a message"
//                                                 },
//                                                 {
//                                                     max: 100,
//                                                     message: "Message should be maximum 20 character long"
//                                                 },
//                                                 {
//                                                     min: 20,
//                                                     message: "Message at least 20 character"
//                                                 },
                                                
//                                             ]}
//                                         >
//                                             <TextArea showCount
//                                             // className="inputformfield"
//                                             style={{border:"1px solid black"}}
//                                             placeholder="Notes"
//                                             autoSize={{ minRows: 3, maxRows: 5 }} 
//                                             maxLength={100} />
//                                         </Form.Item>
//                                     </Col>
//                                 </Row>
//                                 <Row>
//                                     <Col>
//                                         <Button style={{height:"40px",width:"100px", fontSize:"21px"}} type="primary" htmlType="submit">Submit</Button>
//                                     </Col>
//                                 </Row>
//                             </Form>
//                         </Col>
//                     </Row>
//                 </Col>
//             </Row>
//         </>
//     )
// }
