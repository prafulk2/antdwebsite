import React, { useState } from 'react'
import Home from '../Pages/Home'

import { BrowserRouter as Router, Route } from 'react-router-dom'
import SignIn from '../Pages/SignIn'
import SignUp from '../Pages/SignUp'
import Header from './Header'
import Footer from './Footer'
import ContactUs from '../Pages/ContactUs'
import Contact from '../Pages/Contact'
import AboutUs from '../Pages/AboutUs'
import Gallery from '../Pages/Gallery'
import { LoginContext } from '../Helper/Context'


export default function MainCmp() {
    const [login, setLogin] = useState(false)
 
    return (
        <>
            <Router >            
  
                <Header />  
           
                <Route exact path="/Home" component={Home} >
                        <Home />
                </Route>
                <Route exact path="/SignIn" >
                        <SignIn />
                </Route>
                <Route exact path="/SignUp" component={SignUp} >
                        <SignUp />
                </Route>
                <Route exact path="/ContactUs" component={ContactUs} />
                <Route exact path="/AboutUs" component={AboutUs} >
                        <AboutUs />
                </Route>
                <Route exact path="/Contact" component={Contact} >
                        <Contact />
                </Route>
                <Route exact path="/Gallery" component={Gallery} >
                        <Gallery />
                </Route>
                <Route  exact path="/" component={Home} />
                <Footer />
            </Router>
        </>
    )
}
