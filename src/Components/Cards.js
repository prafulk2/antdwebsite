import { Card, Col, Row, Avatar, Modal, Button, Space } from 'antd'
import React, { useState } from 'react'
import './Cards.scss'
import { SecurityScanOutlined ,DownloadOutlined, HeartFilled, HeartOutlined, CalendarOutlined } from '@ant-design/icons';

const { Meta } = Card;
export default function Cards(props) {
    const [visible, setVisible] = useState(false)
    const [like, setLike] = useState(false)
    const img=props.obj.urls.full
    const dateFormate=()=>{
        const today = new Date(props.obj.updated_at);
        const day = today.getDay();
        const month = today.getMonth();
        const year = today.getFullYear();
        const date=day+" / "+month+" / "+year    
        return date
    }

    const getImageid = () => {
        setVisible(!visible)
    }
    const handleLike = () => {
        setLike(!like)
    }

    const handleCancel = () => {
        setVisible(false)
    }
    return (
        <>


            <Modal
                visible={visible}
                onCancel={handleCancel}
                width={"90%"}
                style={{ top: "10px" }}
                className="Modal"
                centered
                header={[
                    <Button>
                        ok
                    </Button>
                ]}
                footer={[
                    ""
                ]}
            >



                <Card
                    style={{ marginTop: "0px" }}
                    className="ModalCard"
                    bordered={false}
                >
                    <div className="ModalCardContent">
                        <Meta
                            className="Content_meta"
                            avatar={<Avatar src={props.obj.user?.profile_image.small} />}
                            title={props?.obj?.user?.name}
                            description={props.obj.alt_description ? props.obj.alt_description : "no description"}
                        />
                        <div className="Button">
                            <div style={{ display: "flex", alignItems: "center" }}>
                                {like ? <HeartFilled className="likes" style={{ color: "red", fontSize: "20px", paddingRight: "6px" }} onClick={handleLike} /> : <HeartOutlined onClick={handleLike} style={{ color: "rgb(178, 171, 171)", fontSize: "20px", paddingRight: "6px" }} />}

                                <Button style={{ marginLeft: "auto", float: "right" }}>
                                    <a  download=""  title="Download photo" href={`https://unsplash.com/photos/${props.obj.id}/download?force=true`}><span>   <DownloadOutlined /> Download </span></a>
                                </Button>
                            </div>
                        </div>
                    </div>
                </Card>

                <Row justify="center">
                    <Col span="16">
                        {img!==""?<img style={{ width: "100%" }} src={props.obj.urls.full} alt="img"></img>: <HeartFilled/>  }
                    </Col>
                </Row>


                <Row justify="center" style={{ marginTop: "20px" }}>
                    <Col span="23">


                        <Row>
                            <Col>
                                <Space>
                                    <CalendarOutlined />
                                    Publish on {dateFormate()}
                                </Space>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row justify="center">
                    <Col span="23">
                        <Space>
                        <SecurityScanOutlined />
                        Free to use under the Unsplash License
                        </Space>
                    </Col>
                </Row>
            </Modal>
            <div span="24" className="cards">
                <img style={{ width: "100%", height: "100%" }} src={props.obj.urls.small} onClick={getImageid} alt="img"></img>
            </div>


        </>
    )
}
