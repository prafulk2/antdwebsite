import { Row, Col, Typography, Space, Divider } from 'antd'
import React from 'react'
import './Footer.scss'
import { TwitterOutlined,InstagramOutlined,DribbbleOutlined  ,PhoneFilled,SkypeOutlined ,LinkedinOutlined,PhoneOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom'

const { Title, Paragraph} = Typography
export default function Footer() {
    return (
        <>
            <Row className="FooterContainer" justify="center">
                <Col span="22">
                    <Row gutter={18}>
                        <Col className="cols" xs={24} sm={14} xl={12}>
                            <Typography className="FootTypography">
                                <Title className="title" level={5}>WHY US</Title>
                                <Row>
                                    <Col xl={20}>
                                    <Paragraph className="para">
                                Creactive Inc. is committed to staying ahead of the curve when it   Creactive Inc. is committed to staying ahead of the curve when it comes to successful internet marketing strategies and techniques. We believe that our companies success is only measured by your companies success!
                                </Paragraph>
                                    </Col>
                                </Row>
                            </Typography>
                        </Col>
                        <Col className="cols" xs={24} sm={6} xl={5} >
                            <Typography className="FootTypography">
                                <Title className="title" level={5}>POPULAR PAGES</Title>
                                <Paragraph className="para">
                                    <Space direction="vertical">
                                        <Link className="Links">Why Us</Link>
                                        <Link className="Links">Check Out Some Website Designs</Link>
                                        <Link className="Links">Pay Per Click Advertising</Link>
                                        <Link className="Links">SEO Services</Link>
                                    </Space>
                                </Paragraph>
                            </Typography>
                        </Col>
                        <Col className="cols" xs={12} sm={14} xl={3}>
                            <Typography className="FootTypography">
                                <Title className="title" level={5}>QUICK LINK</Title>
                                <Paragraph className="para">
                                    <Space direction="vertical">
                                        <Link to="/" className="Links">Home</Link>
                                        <Link to="/Aboutus" className="Links">About Us</Link>
                                        <Link to="/Contact" className="Links" >Contact Us</Link>
                                        <Link to="/Gallery" className="Links" >Gallery</Link>
                                      
                                    </Space>
                                </Paragraph>
                            </Typography>
                        </Col>
                        <Col className="cols" xs={12} sm={6} xl={4}>
                            <Typography className="FootTypography">
                                <Title className="title" level={5}>CONTACT US</Title>
                                <Paragraph className="para">
                                    <Space direction="vertical">
                                    <Link style={{color:"white",margin:"0"}} to="/Contact">Contact Us</Link>
                                    <p style={{margin:"0"}}> <DribbbleOutlined /> xyz@gmail.com</p>
                                    <p style={{margin:"0"}}> <PhoneOutlined /> 972-173-9502</p>
                                  
                                   <Space>
                                   <TwitterOutlined />
                                   <InstagramOutlined />
                                   <LinkedinOutlined />
                                   <PhoneFilled />
                                   <SkypeOutlined />
                                   </Space>
                                    </Space>
                                </Paragraph>
                            </Typography>
                        </Col>
                    </Row>
                    <Divider style={{borderTop:"1px solid white", marginTop:"2px"}}></Divider>
                    <Row justify="center">
                            <Col span="24" align="center">
                                <Typography className="FootTypography">
                                <Paragraph className="para"> Copyright © {new Date().getFullYear()} Odhav Web. All rights reserved Odhav Web.</Paragraph>
                                </Typography>
                            </Col>
                        </Row>
                </Col>
            </Row>
        </>
    )
}
