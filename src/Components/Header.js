import './Header.scss'
import { Button, Col, message, Row, Space, Typography } from 'antd'
import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { handleLogin } from '../Features/Login/LoginSlicer';

const { Title } = Typography
export default function Header() {
    const [btnAlign, setBtnAlign] = useState('end')
    const [btdDirection, setBtdDirection] = useState("horizontal")
    const [isMobail, setIsMobail] = useState(false)
    const [bodyScroll, setBodyScroll] = useState('')
    const logins = useSelector(state => state.login.login)
    const dispatch=useDispatch()
    

    const onClickIcon = () => {
        setIsMobail(!isMobail)
        setBtdDirection(btdDirection === "horizontal" ? 'vertical' : "horizontal")
        setBtnAlign(btnAlign === "end" ? 'center' : 'end')
        setBodyScroll(bodyScroll==="hidden"?'':"hidden")
    }
    const onClickReset = () => {
        setIsMobail(!isMobail)
        setBtnAlign('end')
        setBtdDirection("horizontal")
        setBodyScroll('')
    }
    const handlelogout=()=>{
        dispatch(handleLogin(false))
    }
    const showMessage=()=>{
        if (logins!==true) {
            message.warn("please login first ")
        }
    }

    return (
        <>
    
            <Row justify="center" className="hdcon">
                <Col span="24" >
                    <Row className="mainRow">
                        <Col xs={20} xl={8} sm={22} span="16">
                            <NavLink className="logo" to="/Home"  >
                                <Space >
                                    <img className="thumb image-max-width loaded" alt="navbar img" src="https://cdn-icons-png.flaticon.com/512/187/187879.png" width="35" height="35" />
                                    <Title level={2} className="logotext">Odhav Pvt Ltd</Title>
                                </Space>
                            </NavLink>
                        </Col>
                        <Col xs={4} xl={1} sm={1} span="2" align="end" className="menuicon" onClick={onClickIcon}>
                            <img src="https://img.icons8.com/nolan/64/menu.png" alt="icon" width="43" height="43" />
                        </Col>
                        <Col span="6" xs={24} xl={16} sm={24} align={btnAlign} style={{ alignSelf: "center" }} onClick={onClickReset} className={isMobail ? "Allbtn isMobail" : "Allbtn btnContent"}>
                            <Space direction={btdDirection} className="btnspace">
                                <NavLink  activeClassName="is_active" to="/Home">
                                    <Button className="btn" type="link" style={{ color: "white", width: "85px" }} onClick={showMessage}>
                                        Home
                                    </Button>
                                </NavLink>
                                <NavLink  activeClassName="is_active" to="/Gallery">
                                    <Button className="btn" type="link" style={{ color: "white", width: "85px" }}  onClick={showMessage}>
                                        Gallery
                                    </Button>
                                </NavLink>
                                <NavLink  activeClassName="is_active" to="/AboutUs">
                                    <Button className="btn" type="link" style={{ color: "white" }}  onClick={showMessage}>
                                        About Us
                                    </Button>
                                </NavLink>
                                <NavLink  activeClassName="is_active" to="/Contact">
                                    <Button className="btn" type="link" style={{ color: "white" }}  onClick={showMessage}>
                                        Contact Us
                                    </Button>
                                </NavLink>
                                <NavLink  activeClassName="is_active" to="/SignUp">
                                    <Button className={logins?"btn loginMode":"btn"} type="link" style={{ color: "white" }}>
                                        Sign Up
                                    </Button>
                                </NavLink>
                                <NavLink  activeClassName="is_active" to="/SignIn">
                                    {logins?        
                                    <Button className="btn" type="link" style={{ color: "white" }} onClick={handlelogout}  >
                                        Logout
                                    </Button>:
                                    <Button className="btn" type="link" style={{ color: "white" }}>
                                      Sign In  
                                    </Button>
                                    }
                                </NavLink>
                            </Space>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}
