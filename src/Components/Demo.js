import { Button } from 'antd';
import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { changeTheamColor } from '../Features/Theam/Theamslicer';
export default function Demo() {
    const [color, setColor] = useState('white')
    const dispatch=useDispatch()
    return (
        <>

            <input onChange={(e)=>setColor(e.target.value)} />
            <Button onClick={()=>dispatch(changeTheamColor(color))}>Change Text Color</Button>
            <h1>{color}</h1>
        </>
    )
}
